package com.bejoin;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.jface.viewers.CheckboxTableViewer;

public class Application {
    protected Shell shell;
    private Table table;
    private Table table_1;


    public static void main(String[] args) {

        try {
            Application window = new Application();
            window.open();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Open the window.
     */
    public void open() {
        Display display = Display.getDefault();
        createContents();
        shell.open();
        shell.layout();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
    }

    /**
     * Create contents of the window.
     */
    protected void createContents() {
        shell = new Shell();
        shell.setSize(450, 300);
        shell.setText("SWT Application");
        shell.setLayout(new FillLayout(SWT.VERTICAL));

        Composite composite = new Composite(shell, SWT.H_SCROLL | SWT.V_SCROLL);
        composite.setLayout(new FillLayout(SWT.HORIZONTAL));

        CheckboxTableViewer checkboxTableViewer = CheckboxTableViewer.newCheckList(composite, SWT.BORDER | SWT.FULL_SELECTION);
        table = checkboxTableViewer.getTable();

        Composite composite_1 = new Composite(shell, SWT.H_SCROLL | SWT.V_SCROLL);
        composite_1.setLayout(new FillLayout(SWT.HORIZONTAL));

        table_1 = new Table(composite_1, SWT.BORDER | SWT.FULL_SELECTION);
        table_1.setHeaderVisible(true);
        table_1.setLinesVisible(true);
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        // 创建表头的字符串数组
        String[] tableHeader = {"名称", "型号", "尺寸", "喷射距离","最高时速","简介"};
        for (int i = 0; i < tableHeader.length; i++)
        {
            TableColumn tableColumn = new TableColumn(table, SWT.NONE);
            tableColumn.setText(tableHeader[i]);
            // 设置表头可移动，默认为false
            tableColumn.setMoveable(true);
        }
        //添加一行数据
        TableItem item;
        item = new TableItem(table, SWT.NONE);
        item.setText(new String[]{"重型泡沫车0", " JDF5314GXFPM160" , " 10655*2500*3540" , " 特点：功率大、车速高、载液量大，射程远" , "95 " , " 60"});
        // 添加三行数据
        for (int i = 1; i < 5; i++)
        {
            item = new TableItem(table, SWT.NONE);
            item.setText(new String[]{"重型泡沫车"+i , " JDF5314GXFPM160" , " 10655*2500*3540" , " 特点：功率大、车速高、载液量大，射程远" , "95 " , " 60"});
        }
        // 重新布局表格
        for (int i = 0; i < tableHeader.length; i++)
        {
            table.getColumn(i).pack();
        }
        // 创建表头的字符串数组
        String[] tableHeader1 = {"名称", "坐标"};
        for (int i = 0; i < tableHeader1.length; i++)
        {
            TableColumn tableColumn = new TableColumn(table_1, SWT.NONE);
            tableColumn.setText(tableHeader1[i]);
            // 设置表头可移动，默认为false
            tableColumn.setMoveable(true);
        }
        TableItem item1;
        for(int i=1;i<6;i++){
            item1 = new TableItem(table_1, SWT.NONE);
            item1.setText(new String[]{"重型泡沫车"+i ,2650+3*(i-1)+",8400"});
        }
        for(int i=1;i<5;i++){
            item1 = new TableItem(table_1, SWT.NONE);
            item1.setText(new String[]{"救护车"+i , 2666+3*(i-1)+",8400"});
        }
        // 重新布局表格
        for (int i = 0; i < tableHeader1.length; i++)
        {
            table_1.getColumn(i).pack();
        }

    }

}
